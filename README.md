# Beamer theme for UBB Web Programming slides

## Necessary tools

- a valid [TeXLive](https://www.tug.org/texlive/) distribution

## Installation from repo

```
./install.sh
```

## Downloading packaged version

```bash
wget --output-document=theme.zip https://gitlab.com/bbte-mmi/webprog/theme/-/jobs/artifacts/master/download?job=deploy-artifact
unzip -ud `kpsewhich -var-value=TEXMFHOME` theme.zip
```

## Using the template

```tex
\documentclass{beamer}

% ...

\usetheme{webprog}

\title{Presentation Title}
\subtitle{Subtitle}
\author{Name Surname}

\begin{document}

\begin{frame}
  \maketitle
\end{frame}

% ...

\end{document}
```